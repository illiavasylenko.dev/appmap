<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/_debugbar/assets/stylesheets', [
    'as' => 'debugbar-css',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@css'
]);

Route::get('/_debugbar/assets/javascript', [
    'as' => 'debugbar-js',
    'uses' => '\Barryvdh\Debugbar\Controllers\AssetController@js'
]);


Route::post('sign-in', 'Auth\LoginController@SignIn');
Route::post('sign-up', 'Auth\RegisterController@SignUp');
Route::get('log-out', 'Auth\LoginController@logout');
Route::get('/places/add', 'PlaceController@AddPlace');
Route::post('/places/create', 'PlaceController@create');
Route::get('/places/my-places', 'PlaceController@myPlaces');
Route::get('/places/moderate', 'PlaceController@moderate');
Route::get('/places/edit/{id}', 'PlaceController@editPlace');
Route::post('/places/update/{id}', 'PlaceController@update');
Route::post('/places/remove-image/{id}', 'PlaceController@removeImage');
Route::post('/places/remove-place/{id}', 'PlaceController@removePlace');
Route::get('/places/show/{id}', 'PlaceController@showPlace');
Route::get('/places/moderation/{id}/{status}', 'PlaceController@moderation');
Route::get('/users/all', 'UserController@getAllUsers');
Route::post('/users/changeRole', 'UserController@changeRole');
Route::post('/comment/add', 'CommentController@add');
Route::get('/comments/moderate', 'CommentController@moderate');
Route::get('/comments/my-comments', 'CommentController@myComments');
Route::post('/comments/removeComment', 'CommentController@removeComment');
Route::get('/', 'PlaceController@index');

