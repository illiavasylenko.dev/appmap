var httpPost = function (url, data = {}, generateFD = true) {

    var formData = new FormData();
    if (generateFD)
        Object.keys(data).forEach(function (el) {
           formData.append(el, data[el]);
        });
    else formData = data;


    return fetch('/' + url, {
        method: 'post',
        body: formData,
        headers: {
            'X-CSRF-TOKEN': document.querySelector('input[name=_token]').value
        }
    }).then(function (data) {
        return data.json();
    }).catch(function (e) {
        console.error(e);
        return false;
    })


};



var header = new Vue({
    el: '#header',
    data: {
        modal: false,
        Rmodal: false,
        error: false,
        fName: '',
        lName: '',
        email: 'admin@com.com',
        password: 'password',
        passwordR: 'password',
        btnText: 'Sing In',
        submited: false
    },
    methods: {
        toggleModal: function () {
            this.btnText = 'Sign In';
            this.error = false;
            if (this.Rmodal)
                this.Rmodal = false;
            else
                this.modal = !this.modal;
        },
        singIn: function () {
            var scope = this;
            if (!this.submited)
                if (this.email.length > 5 && this.password.length > 2) {
                    this.submited = true;
                    this.btnText = '...';
                    httpPost('sign-in', {
                        email: this.email,
                        password: this.password
                    }).then(function (data) {
                        scope.submited = false;
                       if (data.logged) {
                           window.location.reload()
                       } else {

                           scope.btnText = 'Sing In';
                           scope.error = true;
                       }
                    });
                } else {
                    this.error = true;
                }
        },
        gotoRegister: function () {
            this.btnText = 'Register';
            this.modal = false;
            this.Rmodal = true;
        },
        goToLogin: function () {
            this.btnText = 'Sing In';
            this.modal = true;
            this.Rmodal = false;
        },
        register: function () {
            var scope = this;
            if (!this.submited)
                if (this.email.length > 5 && this.password.length > 2 && this.password === this.passwordR) {
                    this.submited = true;
                    this.btnText = '...';
                    httpPost('sign-up', {
                        email: this.email,
                        password: this.password,
                        name: this.fName + ' ' + this.lName
                    }).then(function (data) {
                        scope.submited = false;
                        if (data.logged) {
                            window.location.reload()
                        } else {

                            scope.btnText = 'Register';
                            scope.error = true;
                        }
                    });
                } else {
                    this.error = true;
                }
        }
    }
});

if (document.querySelector('#add-place'))
var addPlace = new Vue({
    el: '#add-place',
    data: {
        id: data ? data.id : null,
        map: null,
        marker: null,
        images: [],
        preview: data ? data.images.map(function (image) {
            return {
                id: image.id,
                src: '/public/uploads/' + image.url
            };
        }) : [],
        infoText: 'Click for Add Marker',
        lat: data ? +data.ltd :'',
        lng: data ? +data.lng :'',
        name: data ? data.name :'',
        description: data ? data.description :''
    },

    methods: {
        loadMap: function() {
            this.map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 50.45466, lng: 30.5238},
                zoom: 8
            });
            var scope = this;

            if (data) {
                scope.marker = new google.maps.Marker({map: scope.map});
                scope.marker.setPosition({
                    lat: scope.lat,
                    lng: scope.lng
                });
                scope.infoText = `You pick Lat: ${scope.lat} Lng: ${scope.lng}`;
            }

            google.maps.event.addListener(this.map, 'click', function(e) {

                if (!scope.marker)
                    scope.marker = new google.maps.Marker({map: scope.map});

                scope.marker.setPosition(e.latLng);
                scope.lat = e.latLng.lat();
                scope.lng = e.latLng.lng();
                scope.infoText = `You pick Lat: ${scope.lat} Lng: ${scope.lng}`;
                //console.log(scope.lat, scope.lng);
            });

        },
        uploadImage: function (e) {
            console.log(e);

            var scope = this;


            var i = 0;
            var loaded = 0;
            var w = true;
            while (w) {
                var file = e.target.files[i];
                console.log(file);
                i++;
                if (!file) {w = !w; return false};

                if (file)  this.images.push(file);
                var name = file.name;
                var reader = new FileReader();
                loaded++;
                reader.onload = function(e) {
                    scope.preview.push({
                        file: name,
                        src: e.target.result
                    });
                    loaded--;
                    if (!loaded) {
                        document.querySelector('#file-input').value = null;
                    }
                };
                reader.readAsDataURL(file);
            }

        },
        removeImage: function(img) {
            var image = data && img.id ? data.images.find(function (d) {
                return d.id === img.id;
            }) : null;



            if (image) {
                this.preview = this.preview.filter(function (data) {
                   return data !== img;
                });
                httpPost('places/remove-image/' + image.id).then();
            } else {
                this.preview = this.preview.filter(function (data) {
                    return data !== img;
                });

                this.images = this.images.filter(function (data) {
                    console.log(data.name, img.file);
                    return data.name !== img.file;
                });
            }
        },
        removePlace: function() {
            httpPost('places/remove-place/' + this.id).then(function (data) {
                if (data.result) {
                    window.location.replace('http://' + window.location.hostname + '/places/my-places');
                }
            });
        },
        submit: function () {
            var data = new FormData;
            data.append('name', this.name);
            data.append('description', this.description);
            data.append('lng', this.lng);
            data.append('ltd', this.lat);
            this.images.forEach(function (image) {
                data.append('images[]', image);
            });
            httpPost(this.id ? 'places/update/' + this.id :'places/create', data, false).then(function (data) {
                if (data.result) {
                    window.location.replace('http://' + window.location.hostname + '/places/my-places');
                }
            });
        }
    }
})
if (document.querySelector('#pretty-file-upload'))
document.querySelector('#pretty-file-upload').onclick = function () {
  document.querySelector('#file-input').click();
};



if (document.querySelector('#home-map'))
    var homeMap = new Vue({
       el: '#home-map' ,
        data: {
            map: null,
            markers: [],
            image: null,
            name: null,
            id: null,
            modal: false
        },
        methods: {
            toggleModal: function () {
                this.modal = !this.modal;
            },
            loadMap: function() {
                this.map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 50.45466, lng: 30.5238},
                    zoom: 8
                });
                var scope = this;

                if (data) {

                    data.forEach(function (el) {
                        console.log(el);
                        var marker = new google.maps.Marker({
                            map: scope.map,
                            position: {
                                lat: +el.ltd,
                                lng: +el.lng
                            }
                        });
                        marker.addListener('click', function () {
                            scope.name = el.name;
                            scope.description = el.description;
                            scope.image = el.mainImage;
                            scope.id = el.id;
                            scope.modal = true;
                        });
                        scope.markers.push(marker);
                    });

                }



            },
        }
    });
function initMap() {

    addPlace ? addPlace.loadMap() :
        homeMap ? homeMap.loadMap() :
            initSingle ? initSingle() :
                null;
}


if (document.querySelector('#gallery'))
    var gallery = new Vue({
        el: '#gallery',
        data: {
            images: gallery,
            current: gallery[0],
            modal: false
        },
        methods: {
            change: function (id) {
                console.log(id);
                this.current = this.images.find(function (el) {
                    return el.id === id;
                });
            },
            toggleModal: function () {
                this.modal = !this.modal;
            },
        }
    })


if (document.querySelector('#usersTable'))
    var usersTable = new Vue({
       el: '#usersTable',
       data: {
            users: data ? data.map(function (user) {
                user.role = user.role ? user.role.name : 'user';

                return user;
            }) : []
       },
        methods: {
            save: function () {
                httpPost('users/changeRole', {
                    data: JSON.stringify(this.users)
                }).then();
            }
        }
    });


if (document.querySelector('#comments'))
    var comments = new Vue({
        el: '#comments',
        data: {
            comments: comments ? comments.map(function (comment) {

                comment.name = comment.user ? comment.user.name : comment.anonimous_user_name;
                comment.user = comment.user ? comment.user.id : null;
                return comment;
            }) : []
        },
        methods: {
            add: function (comment) {
                this.comments.push(comment)
            },
            removeComment: function (id) {
                var scope = this;
                httpPost('comments/removeComment', {
                    id: id
                }).then(function (data) {
                    if (data.result) {
                        scope.comments = scope.comments.filter(function (el) {
                            return el.id !== id;
                        })
                    }
                });
            }
        }
    });
if (document.querySelector('#commentAdd'))
    var commentAdd = new Vue({
       el: '#commentAdd',
       data: {
           username: user ? user : '',
           title: '',
           text: '',
           place_id: place,
           btnText: 'Submit',
           submited: false
       },
        methods: {
            save: function () {
                var scope = this;
                this.btnText = '...';
                if (!this.submited) {
                    this.submited = true;
                    httpPost('comment/add', {
                        username: this.username,
                        title: this.title,
                        text: this.text,
                        place_id: this.place_id
                    }).then(function (data) {
                        scope.submited = false;
                        scope.btnText = 'Submit';
                        if (data.result) {
                            scope.title = '';
                            scope.text = '';
                            if (comments) {
                                data.comment.name = scope.username;
                                comments.add(data.comment);
                            }
                        }
                    });
                }

            }
        }
    });
