<?php

namespace App\Providers;

use App\models\UserRole;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->roles = UserRole::all();
        $this->registerPolicies();

        Gate::define('administrate', function ($user) {
            foreach ($this->roles as $role ) {

                if ($role->user_id === $user->id && $role->name == 'admin') {
                    return true;
                }
            }

            return false;
        });
        Gate::define('moderate', function ($user) {
            foreach ($this->roles as $role ) {

                if ($role->user_id === $user->id && ( $role->name == 'admin' ||  $role->name == 'moderator')) {
                    return true;
                }
            }
            return false;
        });
    }
}
