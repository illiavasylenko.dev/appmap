<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    //


    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments() {
        return $this->hasMany('App\Models\Comment');
    }

    public function images() {
        return $this->hasMany('App\Models\Gallery');
    }
}
