<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public function place()
    {
        return $this->belongsTo('App\Models\Place');
    }
}
