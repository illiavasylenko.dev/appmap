<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
