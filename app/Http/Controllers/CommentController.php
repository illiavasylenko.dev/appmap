<?php

namespace App\Http\Controllers;

use App\models\Comment;
use Gate;
use Illuminate\Http\Request;

class CommentController extends Controller
{


    public function add () {

        if ($_POST['place_id']) {
            $next = true;
            $comment = new Comment();
            $comment->place_id = $_POST['place_id'];
            $comment->title = $_POST['title'];
            $comment->text = $_POST['text'];

            if (auth()->user()) {
                $comment->user_id = auth()->user()->id;
                $comment->anonimous_user_name ='';
            } else {
                if ($_POST['username'])
                    $comment->anonimous_user_name = $_POST['username'];
                else $next = false;
            }

            if ($next) {
                $comment->save();
                return [
                    'result' => true,
                    'comment' => $comment->toArray()
                ];
            }

        }

        return ['result' => false];
    }
    public function removeComment() {
        if (Gate::allows('moderate')){

            $comment = Comment::where(['user_id' => auth()->user()->id, 'id' => $_POST['id']])->get()[0];
            if ($comment) {
                $comment->delete();
                return ['result' => true];
            }

        } else {
            $comment = Comment::where(['user_id' => auth()->user()->id, 'id' => $_POST['id']])->get()[0];
            if ($comment) {
                $comment->delete();
                return ['result' => true];
            }

        }
        return ['result' => false];
    }
    public function moderate(Request $request)
    {
        if (Gate::allows('moderate')) {
            $comments = Comment::all();

            return view('comments/moderate', ['comments' => $comments]);
        }



    }
    public function myComments() {

            $comments = Comment::where(['user_id' => auth()->user()->id])->get();

            return view('comments/moderate', ['comments' => $comments]);

    }
}
