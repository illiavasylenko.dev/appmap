<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\models\UserRole;
use Gate;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //


    function getAllUsers() {
        if (Gate::allows('administrate')) {
            $users = User::all();

            foreach ($users as $user) {
                $user->role = $user->role;
            }
            return view('/users/all', ['users' => $users]);
        }
    }
    function changeRole() {
        if (Gate::allows('administrate')) {
            $users = User::all();
            $data = json_decode($_POST['data']);

            foreach ($users as $user) {
                foreach ($data as $u) {
                    if ($user->id == $u->id) {

                        if (!$user->role && $u->role !== 'user') {
                            $role = new UserRole();
                            $role->name = $u->role;
                            $role->user_id = $user->id;
                            $role->save();
                        }else if ($user->role->name !== $u->role) {
                            $user->role->name = $u->role;
                            $user->role->save();
                        }
                    }
                }
            }

            return ['data' => true];
        }
    }
}
