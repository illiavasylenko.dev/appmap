<?php

namespace App\Http\Controllers;

use App\models\Gallery;
use App\models\Place;
use Gate;
use Illuminate\Http\Request;

class PlaceController extends Controller
{

    public static $API_KEY = 'AIzaSyAOFIXEcmrw5Dy6S6bxUfxuTHaXpB9rmC0';

    public function index() {

        $places = Place::where(['status' => 'ACCEPTED'])->get();
        foreach ($places as $place) {


            $place->mainImage = count($place->images) ? $place->images[0]['url'] : null;
        }
        return view('place/index', ['places' => $places]);
    }

    public function AddPlace() {
        return view('place/form', ['data' => false]);
    }
    public function create(Request $request) {

        if($request->hasfile('images'))
        {
            $place = new Place();
            $place->user_id = auth()->user()->id;
            $place->name = $_POST['name'];
            $place->description = $_POST['description'];
            $place->lng = $_POST['lng'];
            $place->ltd = $_POST['ltd'];
            $place->status = 'PENDING';
            $place->popularity = 0;

            $place->save();

            $request->validate([

                'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            ]);
            foreach($request->file('images') as $image)
            {
                $name=time().'.'.$image->getClientOriginalName();
                $image->move(public_path().'/uploads/', $name);
                $image= new Gallery();
                $image->url = $name;
                $image->place_id = $place->id;
                $image->save();
            }
            return ['result' => true];
        } else  return ['result' => false];

    }


    public function update(Request $request, $id) {


            $place = Place::where(['user_id' => auth()->user()->id, 'id' =>$id])->get()[0];

            $place->name = $_POST['name'];
            $place->description = $_POST['description'];
            $place->popularity = $_POST['lng'] == $place->lng && $place->ltd == $_POST['ltd'] ? $place->popularity : 0;
            $place->lng = $_POST['lng'];
            $place->ltd = $_POST['ltd'];
            $place->status = 'PENDING';


            $place->save();


        if($request->hasfile('images'))
        {
            $request->validate([

                'images.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg',

            ]);
            foreach($request->file('images') as $image)
            {
                $name=time().'.'.$image->getClientOriginalName();
                $image->move(public_path().'/uploads/', $name);
                $image= new Gallery();
                $image->url = $name;
                $image->place_id = $place->id;
                $image->save();
            }
        }
            return ['result' => true];


    }

    public function myPlaces() {
        $places = Place::where(['user_id' => auth()->user()->id])->get();

        foreach ($places as $place) {
            $place->mainImage = count($place->images) ? $place->images[0]['url'] : null;
        }
        return view('place/my-places', ['places' => $places]);
    }

    public function editPlace(Request $request, $id)
    {
        $place = Place::where(['user_id' => auth()->user()->id, 'id' => $id])->with('images')->get();
        return view('place/form', ['data' => $place[0]]);
    }
    public function removeImage(Request $request, $id) {
        $img = Gallery::where(['id' =>$id])->with('place')->get()[0];
        if ($img->place->user_id == auth()->user()->id)
            $img->delete();

        return ['data' => true];
    }
    public function removePlace(Request $request, $id) {
        $place = Place::where(['user_id' => auth()->user()->id, 'id' => $id])->get()[0];
        if ($place)
            $place->delete();

        return ['result' => true];
    }
    public function showPlace(Request $request, $id)
    {
        if (Gate::allows('moderate'))
            $place = Place::where([ 'id' => $id])->with('images')->get();
        else
            $place = Place::where(['status' => 'ACCEPTED', 'id' => $id])->with('images')->get();

        return view('place/view', ['data' => $place[0]]);
    }

    public function moderate(Request $request)
    {
        $places = Place::where('status', 'PENDING')->get();
        return view('place/my-places', ['places' => $places]);
    }
    public function moderation(Request $request, $id, $status)
    {
        if (Gate::allows('moderate')) {
            $place = Place::where(['id' => $id])->get()[0];
            $place->status = $status == 'success' ? 'ACCEPTED' : 'DECLINED';
            $place->save();

            return redirect('/places/moderate');
        }

    }
}
