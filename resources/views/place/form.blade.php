@extends('app')

@section('content')


    <div class="container">

        <form id="add-place" v-on:submit.prevent="submit">
            <div class="row">
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" v-model="name" class="form-control">
                    </div>
                    <div class="gallery">
                        <div  class="row">
                            <div class="col-md-4" v-for="image in preview" style="margin-bottom: 20px;">
                                <button v-on:click="removeImage(image)" type="button" class="close" aria-label="Close" style="position: absolute; color:red;right: 20px">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <img :src="image.src" alt="" class="img-fluid rounded">
                            </div>
                            <div class="col-md-4">
                                <label >Upload Image</label>
                                <img src="https://www.synamedia.com/wp-content/uploads/2019/04/cloud-upload-icon.png" alt="" id="pretty-file-upload" class="img-fluid rounded" style="cursor: pointer">
                            </div>
                        </div>
                    </div>
                    <input type="file" accept="image/*" @change="uploadImage($event)" id="file-input" multiple style="display: none">

                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <textarea class="form-control" rows="5" v-model="description"></textarea>
                    </div>





                </div>
                <div class="col-md-6">

                    <div id="map" style="width: 100%;height: 600px"></div>
                    <div class="info">
                        @{{ infoText }}
                    </div>
                </div>
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    @if ($data)
                        <button type="button" class="btn btn-danger" v-on:click="removePlace">Remove</button>
                    @endif
                </div>
            </div>
        </form>
        <script>
            var data = JSON.parse('<?=json_encode($data)?>')
        </script>
    </div>
@endsection
