@extends('app')

@section('content')


        <section class="text-center" id="home-map">

                <div id="map" style="width: 100%;height: 600px"></div>
            <div class="modal" tabindex="-1" role="dialog" style="display: block" v-if="modal">
                <div class="modal-dialog" role="document" >
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">@{{ name }}</h5>
                            <button type="button" class="close"  aria-label="Close" v-on:click="toggleModal">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img :src="'/public/uploads/' + image" alt="" class="img-fluid">
                            <a :href="'/places/show/' + id" class="btn btn-default">Show More</a>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">

                <div class="row">
                    @foreach ($places as $place)
                        @component('components.card', ['place' => $place]) @endcomponent

                    @endforeach
                </div>
            </div>
        </div>
        <script>
            var data = JSON.parse('<?=json_encode($places)?>')
        </script>
@endsection
