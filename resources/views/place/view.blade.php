@extends('app')

@section('content')
   <div class="container">
       <div class="row">
           <div class="col-md-6">


               <div id="gallery">
                   <div class="container">
                       <div class="row">
                           <div class="col-md-12">
                               <h1>{{ $data['name'] }}</h1>
                               @if (Gate::allows('moderate') && $data['status'] == 'PENDING')
                                   <a class="btn btn-success" href="/places/moderation/{{$data['id']}}/success">ACCEPT </a>
                                   <a class="btn btn-danger" href="/places/moderation/{{$data['id']}}/decline">DECLINE </a>
                               @endif
                               <div class="current">
                                   <img :src="'/public/uploads/' + current.url" alt="" class="img-fluid" v-on:click="toggleModal">
                               </div>
                               <div class="all">
                                   <div class="row" style="margin-top: 10px;">
                                       <div class="col-md-2" v-for="image in images" v-on:click="change(image.id)">
                                           <img :src="'/public/uploads/' + image.url" alt="" class="img-fluid rounded" style="cursor: pointer" v-if="image.id !== current.id">
                                           <img :src="'/public/uploads/' + image.url" alt="" class="img-fluid rounded"  style="cursor: pointer;border:1px solid blue;" v-if="image.id === current.id">
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="modal" tabindex="-1" role="dialog" style="display: block" v-if="modal" v-on:click="toggleModal">
                       <div class="modal-dialog" role="document" >
                           <div class="modal-content">

                               <div class="modal-body">
                                   <img :src="'/public/uploads/' + current.url" alt="" class="img-fluid">
                               </div>

                           </div>
                       </div>
                   </div>
               </div>
               @component('components.comment', ['place_id' => $data['id'], 'comments' => $data->comments]) @endcomponent

           </div>
           <div class="col-md-6">
               <div id="map" style="width: 100%;height: 600px"></div>
           </div>
       </div>
   </div>
   <script>
       var gallery = JSON.parse('<?=json_encode($data['images'])?>')
       function initSingle() {
           var map = new google.maps.Map(document.getElementById('map'), {
               center: {lat: +'<?=$data['ltd']?>',lng: +'<?=$data['lng']?>'},
               zoom: 8
           });




               var marker = new google.maps.Marker({
                   map: map,
                    position: {
                       lat: +'<?=$data['ltd']?>',lng: +'<?=$data['lng']?>'
                    }
               });
       }
   </script>
@endsection
