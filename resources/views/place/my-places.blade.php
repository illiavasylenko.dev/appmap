@extends('app')

@section('content')

  <div class="container">
      <div class="row">
          @foreach ($places as $place)
              @component('components.card', ['place' => $place]) @endcomponent

          @endforeach
      </div>
  </div>


@endsection
