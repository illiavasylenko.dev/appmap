<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <!-- Fonts -->

    <!-- Styles -->

</head>
<body>
@csrf
@component('components.header') @endcomponent
<main role="main">
@yield('content')
</main>
</body>



<script src="https://cdn.jsdelivr.net/npm/vue@2.6.11"></script>
<script src="/public/js/script.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=\App\Http\Controllers\PlaceController::$API_KEY?>&callback=initMap"
        async defer></script>
</html>
