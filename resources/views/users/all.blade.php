@extends('app')

@section('content')

    <div class="container">
        <div class="row" id="usersTable">
            <table class="table" >
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Role</th>
                </tr>
                </thead>
                <tbody>

                    <tr v-for="user in users">
                        <td>@{{user.id}}</td>
                        <td>@{{user.name}}</td>
                        <td>@{{user.email}}</td>

                       <td>
                           <select v-model="user.role">
                               <option value="admin">Admin</option>
                               <option value="moderator">Moderator</option>
                               <option value="user">User</option>
                           </select>
                       </td>

                    </tr>



                </tbody>
            </table>
            <button class="btn btn-primary" v-on:click="save">Save</button>
        </div>
    </div>
    <script>
        var data = JSON.parse('<?=json_encode($users)?>');
    </script>

@endsection
