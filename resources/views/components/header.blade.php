<div id="header"
     class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm"
     xmlns:v-on="http://www.w3.org/1999/xhtml">
    <h5 class="my-0 mr-md-auto font-weight-normal">APP</h5>
    <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="/">Home</a>




    @auth
        <a class="p-2 text-dark" href="/places/add">Add a Place</a>
        <a class="p-2 text-dark" href="/places/my-places">My Places</a>
        <a class="p-2 text-dark" href="/comments/my-comments">My Comments</a>
            @if (Gate::allows('moderate'))
                <a class="p-2 text-dark" href="/places/moderate">Moderate Places </a>
                <a class="p-2 text-dark" href="/comments/moderate">Moderate Comments </a>
            @endif
        @if (Gate::allows('administrate'))
                <a class="p-2 text-dark" href="/users/all">Users</a>
        @endif
                <a href="/log-out" class="btn btn-outline-primary">Log Out</a>
    @endauth
    @guest

    <a class="p-2 text-dark"  v-on:click.prevent="toggleModal" href="#">Add a Place</a>

    <a href="#" class="btn btn-outline-primary"  v-on:click.prevent="toggleModal">Sign In</a>
    </nav>
    <div class="modal" tabindex="-1" role="dialog" style="display: block" v-if="modal">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sign In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="toggleModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" v-on:submit.prevent="singIn">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" v-model="email" class="form-control" aria-describedby="emailHelp">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" v-model='password' class="form-control" >
                            <div class="invalid-feedback" style="display: block" v-if="error">
                                Email or password is incorrect!
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">@{{ btnText }}</button>
                        <button type="button" class="btn btn-default" v-on:click="gotoRegister">Register</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" tabindex="-1" role="dialog" style="display: block" v-if="Rmodal">
        <div class="modal-dialog" role="document" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Sign In</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="toggleModal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" v-on:submit.prevent="register">
                    <form>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" v-model="email" class="form-control" aria-describedby="emailHelp">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">First Name</label>
                            <input type="text" v-model="fName" class="form-control">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" v-model="lName" class="form-control">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" v-model='password' class="form-control" >

                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Repeat Password</label>
                            <input type="password" v-model='passwordR' class="form-control" >
                            <div class="invalid-feedback" style="display: block" v-if="error">
                                Email or password is incorrect!
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">@{{ btnText }}</button>
                        <button type="button" class="btn btn-default" v-on:click="goToLogin">I am already registered</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
    @endguest
</div>
