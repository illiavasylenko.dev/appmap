@if ($place)
<div class="col-md-4">
    <div class="card mb-4 shadow-sm">
        <img src="/public/uploads/{{$place['mainImage']}}" alt="" class="bd-placeholder-img card-img-top">
        <div class="card-body">
            <p class="card-text">{{$place['name']}}</p>
            <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                    @if ($place['status'] == 'ACCEPTED' || Gate::allows('moderate'))
                        <a href="/places/show/{{$place['id']}}" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                    @endif

                    @auth
                        @if (auth()->user()->id == $place['user_id'])
                            <a href="/places/edit/{{$place['id']}}" type="button" class="btn btn-sm btn-outline-secondary">Edit</a>
                        @endif
                     @endauth
                </div>
                @auth
                @if (auth()->user()->id == $place['user_id'])
                    @if ($place['status'] == 'PENDING')
                        <small class="text-muted">{{$place['status']}}</small>
                    @endif
                    @if ($place['status'] == 'ACCEPTED')
                        <small class="text-success">{{$place['status']}}</small>
                    @endif
                    @if ($place['status'] == 'DECLINED')
                        <small class="text-danger">{{$place['status']}}</small>
                    @endif
                @endif
                @endauth
            </div>
        </div>
    </div>
</div>
@endif
