<?php
    // place_id
    // comments
?>





<div id="comments" style="margin-top: 20px;">
    <h2>Comments: @{{ comments.length }}</h2>
    <div class="media" v-for="comment in comments">
        <div class="media-body" >
            @if (Gate::allows('moderate'))
            <button v-on:click="removeComment(comment.id)" type="button" class="close" aria-label="Close" style="position: absolute; color:red;right: 20px">
                <span aria-hidden="true">&times;</span>
            </button>
            @endif
            @auth
            <button v-on:click="removeComment(comment.id)" type="button" class="close" aria-label="Close" style="position: absolute; color:red;right: 20px" v-if="comment.user === {{auth()->user()->id}}">
                <span aria-hidden="true">&times;</span>
            </button>
            @endauth
            <h5 class="mt-0">@{{comment.name}} - @{{comment.title}}</h5>
            <p>
                @{{comment.text}}
            </p>
        </div>
    </div>
</div>

@if ($place_id)
    @component('components.commentForm', ['place_id' => $place_id]) @endcomponent
@endif


<?php
foreach ($comments as $comment) {
    $comment->user = $comment->user;
}
?>
<script >
    var comments = JSON.parse('<?=json_encode($comments)?>');
</script>
