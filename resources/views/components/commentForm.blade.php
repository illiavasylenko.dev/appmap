<form id="commentAdd" v-on:submit.prevent="save">
    @guest
    <div class="form-group">
        <label for="exampleInputEmail1">Your Name</label>
        <input type="text" v-model="username" class="form-control">
    </div>
    @endguest
    <div class="form-group">
        <label for="exampleInputEmail1">Yout Title</label>
        <input type="text" v-model="title" class="form-control">

    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Your Text</label>
        <textarea type="text" v-model="text" class="form-control"></textarea>

    </div>
    <button type="submit" class="btn btn-primary">@{{  btnText }}</button>
</form>


<script>
    var place = '<?=$place_id?>';
    var user = null;
</script>
@auth
<script>
    user = '<?=auth()->user()->name?>';
</script>
@endauth
