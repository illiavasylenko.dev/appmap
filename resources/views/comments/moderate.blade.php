@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @component('components.comment', ['comments' => $comments, 'place_id'=> false]) @endcomponent

            </div>

        </div>
    </div>

@endsection
